-- MySQL dump 10.13  Distrib 5.5.56, for Linux (x86_64)
--
-- Host: localhost    Database: impactapp
-- ------------------------------------------------------
-- Server version	5.5.56

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblCatagory`
--

DROP TABLE IF EXISTS `tblCatagory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCatagory` (
  `clmId` int(11) NOT NULL AUTO_INCREMENT,
  `clmCatagory` varchar(255) DEFAULT NULL,
  `clmDescription` varchar(255) DEFAULT NULL,
  `clmActive` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`clmId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblCatagory`
--

LOCK TABLES `tblCatagory` WRITE;
/*!40000 ALTER TABLE `tblCatagory` DISABLE KEYS */;
INSERT INTO `tblCatagory` VALUES (1,'Animals','Animals',NULL),(2,'Fruit','Fruit',NULL),(3,'Vegetables','Vegetables',NULL),(4,'Food','Food',NULL),(5,'Beverages','Beverages',NULL),(6,'Kitchenware','Kitchenware',NULL),(7,'Household',NULL,NULL),(8,'Clothes',NULL,NULL),(9,'Electronics',NULL,NULL),(10,'Places',NULL,NULL),(11,'Vehicles',NULL,NULL),(12,'Toys',NULL,NULL),(13,'Letters',NULL,NULL),(14,'Numbers',NULL,NULL),(15,'Plants',NULL,NULL),(16,'Outdoors',NULL,NULL),(17,'Body',NULL,NULL);
/*!40000 ALTER TABLE `tblCatagory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblCatagoryLookup`
--

DROP TABLE IF EXISTS `tblCatagoryLookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCatagoryLookup` (
  `clmId` int(11) NOT NULL AUTO_INCREMENT,
  `clmCatagory` varchar(6) DEFAULT NULL,
  `clmWord` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`clmId`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblCatagoryLookup`
--

LOCK TABLES `tblCatagoryLookup` WRITE;
/*!40000 ALTER TABLE `tblCatagoryLookup` DISABLE KEYS */;
INSERT INTO `tblCatagoryLookup` VALUES (1,'1','1'),(2,'1','2'),(3,'1','3'),(4,'1','4'),(5,'1','5'),(6,'1','6'),(7,'1','7'),(8,'1','8'),(9,'1','9'),(10,'1','10'),(11,'1','11'),(12,'1','12'),(13,'1','13'),(14,'1','14'),(15,'1','15'),(16,'1','16'),(17,'1','17'),(18,'2','18'),(19,'2','19'),(20,'2','20'),(21,'2','21'),(22,'2','22'),(23,'2','23'),(24,'2','24'),(25,'2','25'),(26,'2','26'),(27,'3','27'),(28,'3','28'),(29,'3','29'),(30,'3','30'),(31,'3','31'),(32,'3','32'),(33,'4','33'),(34,'4','34'),(35,'4','35'),(36,'4','36'),(37,'4','37'),(38,'4','38'),(39,'4','39'),(40,'4','40'),(41,'4','41'),(42,'4','42'),(43,'4','43'),(44,'4','44'),(45,'5','45'),(46,'5','46'),(47,'5','47'),(48,'5','48'),(49,'5','49'),(50,'5','50'),(51,'6','51'),(52,'6','52'),(53,'6','53'),(54,'6','54'),(55,'6','55'),(56,'6','56'),(57,'6','57'),(58,'6','58'),(59,'7','59'),(60,'7','60'),(61,'7','61'),(62,'7','62'),(63,'7','63'),(64,'7','64'),(65,'7','65'),(66,'7','66'),(67,'7','67'),(68,'7','68'),(69,'7','69'),(70,'7','70'),(71,'7','71'),(72,'7','72'),(73,'7','73'),(74,'7','74'),(75,'8','75'),(76,'8','76'),(77,'8','77'),(78,'8','78'),(79,'8','79'),(80,'8','80'),(81,'8','81'),(82,'8','82'),(83,'8','83'),(84,'8','84'),(85,'8','85'),(86,'8','86'),(87,'8','87'),(88,'8','88'),(89,'8','89'),(90,'8','90'),(91,'8','91'),(92,'8','92'),(93,'8','93'),(94,'8','94'),(95,'8','95'),(96,'8','96'),(97,'9','97'),(98,'9','98'),(99,'9','99'),(100,'9','100'),(101,'9','101'),(102,'9','102'),(103,'9','103'),(104,'9','104'),(105,'10','105'),(106,'10','106'),(107,'10','107'),(108,'10','108'),(109,'11','109'),(110,'11','110'),(111,'11','111'),(112,'11','112'),(113,'11','113'),(114,'12','114'),(115,'12','115'),(116,'12','116'),(117,'12','117'),(118,'12','118'),(119,'12','119'),(120,'12','120'),(121,'13','121'),(122,'13','122'),(123,'13','123'),(124,'13','124'),(125,'13','125'),(126,'13','126'),(127,'13','127'),(128,'13','128'),(129,'13','129'),(130,'13','130'),(131,'13','131'),(132,'13','132'),(133,'13','133'),(134,'13','134'),(135,'13','135'),(136,'13','136'),(137,'13','137'),(138,'13','138'),(139,'13','139'),(140,'13','140'),(141,'13','141'),(142,'13','142'),(143,'13','143'),(144,'13','144'),(145,'13','145'),(146,'13','146'),(147,'13','147'),(148,'14','148'),(149,'14','149'),(150,'14','150'),(151,'14','151'),(152,'14','152'),(153,'14','153'),(154,'14','154'),(155,'14','155'),(156,'14','156'),(157,'14','157'),(158,'14','158'),(159,'15','159'),(160,'15','160'),(161,'15','161'),(162,'15','162'),(163,'16','163'),(164,'16','164'),(165,'16','165'),(166,'16','166'),(167,'17','167'),(168,'17','168'),(169,'17','169'),(170,'17','170'),(171,'17','171'),(172,'17','172'),(173,'17','173'),(174,'17','174'),(175,'17','175'),(176,'17','176'),(177,'17','177'),(178,'17','178'),(179,'17','179'),(180,'17','180');
/*!40000 ALTER TABLE `tblCatagoryLookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblWords`
--

DROP TABLE IF EXISTS `tblWords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblWords` (
  `clmId` int(11) NOT NULL AUTO_INCREMENT,
  `clmWord` varchar(255) DEFAULT NULL,
  `clmDescription` varchar(255) DEFAULT NULL,
  `clmActive` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`clmId`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblWords`
--

LOCK TABLES `tblWords` WRITE;
/*!40000 ALTER TABLE `tblWords` DISABLE KEYS */;
INSERT INTO `tblWords` VALUES (1,'Dog',NULL,NULL),(2,'Cat',NULL,NULL),(3,'Squirrel',NULL,NULL),(4,'Bird',NULL,NULL),(5,'Pig',NULL,NULL),(6,'Horse',NULL,NULL),(7,'Chicken',NULL,NULL),(8,'Snake',NULL,NULL),(9,'Alligator',NULL,NULL),(10,'Bug',NULL,NULL),(11,'Rabbit',NULL,NULL),(12,'Bear',NULL,NULL),(13,'Fox',NULL,NULL),(14,'Wolf',NULL,NULL),(15,'Rat',NULL,NULL),(16,'Mouse',NULL,NULL),(17,'Animal',NULL,NULL),(18,'Apple',NULL,NULL),(19,'Orange',NULL,NULL),(20,'Grape',NULL,NULL),(21,'Pear',NULL,NULL),(22,'Strawberry',NULL,NULL),(23,'Blueberry',NULL,NULL),(24,'Blackberry',NULL,NULL),(25,'Raspberry',NULL,NULL),(26,'Tomato',NULL,NULL),(27,'Vegetable',NULL,NULL),(28,'Carrot',NULL,NULL),(29,'Radish',NULL,NULL),(30,'Fruit',NULL,NULL),(31,'Cucumber',NULL,NULL),(32,'Potato',NULL,NULL),(33,'Food',NULL,NULL),(34,'Meat',NULL,NULL),(35,'Chicken',NULL,NULL),(36,'Pork',NULL,NULL),(37,'Beef',NULL,NULL),(38,'Sausage',NULL,NULL),(39,'Candy',NULL,NULL),(40,'Cake',NULL,NULL),(41,'Cupcake',NULL,NULL),(42,'Cookie',NULL,NULL),(43,'Donut',NULL,NULL),(44,'Pastry',NULL,NULL),(45,'Water',NULL,NULL),(46,'Juice',NULL,NULL),(47,'Soda',NULL,NULL),(48,'Smoothie',NULL,NULL),(49,'Shake',NULL,NULL),(50,'Coffee',NULL,NULL),(51,'Fork',NULL,NULL),(52,'Knife',NULL,NULL),(53,'Spoon',NULL,NULL),(54,'Napkin',NULL,NULL),(55,'Plate',NULL,NULL),(56,'Cup',NULL,NULL),(57,'Bowl',NULL,NULL),(58,'Chopsticks',NULL,NULL),(59,'Kitchen',NULL,NULL),(60,'House',NULL,NULL),(61,'Room',NULL,NULL),(62,'Bed',NULL,NULL),(63,'Dresser',NULL,NULL),(64,'Closet',NULL,NULL),(65,'Table',NULL,NULL),(66,'Bathroom',NULL,NULL),(67,'Home',NULL,NULL),(68,'Chair',NULL,NULL),(69,'Window',NULL,NULL),(70,'Mirror',NULL,NULL),(71,'Light',NULL,NULL),(72,'Lamp',NULL,NULL),(73,'Switch',NULL,NULL),(74,'Refrigerator',NULL,NULL),(75,'Clothes',NULL,NULL),(76,'Shirt',NULL,NULL),(77,'Pants',NULL,NULL),(78,'Shorts',NULL,NULL),(79,'Sock',NULL,NULL),(80,'Underwear',NULL,NULL),(81,'Pantie',NULL,NULL),(82,'Boxer',NULL,NULL),(83,'Bra',NULL,NULL),(84,'Undershirt',NULL,NULL),(85,'Blouse',NULL,NULL),(86,'Dress',NULL,NULL),(87,'Shoes',NULL,NULL),(88,'Jacket',NULL,NULL),(89,'Sweater',NULL,NULL),(90,'Hat',NULL,NULL),(91,'Cap',NULL,NULL),(92,'Beanie',NULL,NULL),(93,'Watch',NULL,NULL),(94,'Slipper',NULL,NULL),(95,'Sandal',NULL,NULL),(96,'Necklace',NULL,NULL),(97,'Phone',NULL,NULL),(98,'iPhone',NULL,NULL),(99,'Tablet',NULL,NULL),(100,'iPad',NULL,NULL),(101,'Computer',NULL,NULL),(102,'Laptop',NULL,NULL),(103,'Clock',NULL,NULL),(104,'Timer',NULL,NULL),(105,'Place',NULL,NULL),(106,'Restaurant',NULL,NULL),(107,'Grocery',NULL,NULL),(108,'Store',NULL,NULL),(109,'Car',NULL,NULL),(110,'Bike',NULL,NULL),(111,'Scooter',NULL,NULL),(112,'Skateboard',NULL,NULL),(113,'Airplane',NULL,NULL),(114,'Toy',NULL,NULL),(115,'Bubble',NULL,NULL),(116,'Figet Spinner',NULL,NULL),(117,'Trampoline',NULL,NULL),(118,'Ball',NULL,NULL),(119,'Frisbee',NULL,NULL),(120,'Jump Rope',NULL,NULL),(121,'Letter',NULL,NULL),(122,'A',NULL,NULL),(123,'B',NULL,NULL),(124,'C',NULL,NULL),(125,'D',NULL,NULL),(126,'E',NULL,NULL),(127,'F',NULL,NULL),(128,'G',NULL,NULL),(129,'H',NULL,NULL),(130,'I',NULL,NULL),(131,'J',NULL,NULL),(132,'K',NULL,NULL),(133,'L',NULL,NULL),(134,'M',NULL,NULL),(135,'N',NULL,NULL),(136,'O',NULL,NULL),(137,'P',NULL,NULL),(138,'Q',NULL,NULL),(139,'R',NULL,NULL),(140,'S',NULL,NULL),(141,'T',NULL,NULL),(142,'U',NULL,NULL),(143,'V',NULL,NULL),(144,'W',NULL,NULL),(145,'X',NULL,NULL),(146,'Y',NULL,NULL),(147,'Z',NULL,NULL),(148,'Number',NULL,NULL),(149,'One',NULL,NULL),(150,'Two',NULL,NULL),(151,'Three',NULL,NULL),(152,'Four',NULL,NULL),(153,'Five',NULL,NULL),(154,'Six',NULL,NULL),(155,'Seven',NULL,NULL),(156,'Eight',NULL,NULL),(157,'Nine',NULL,NULL),(158,'Ten',NULL,NULL),(159,'Tree',NULL,NULL),(160,'Bush',NULL,NULL),(161,'Plant',NULL,NULL),(162,'Flower',NULL,NULL),(163,'River',NULL,NULL),(164,'Lake',NULL,NULL),(165,'Ocean',NULL,NULL),(166,'Desert',NULL,NULL),(167,'Tattoo',NULL,NULL),(168,'Head',NULL,NULL),(169,'Hair',NULL,NULL),(170,'Arm',NULL,NULL),(171,'Leg',NULL,NULL),(172,'Foot',NULL,NULL),(173,'Feet',NULL,NULL),(174,'Finger',NULL,NULL),(175,'Toe',NULL,NULL),(176,'Eyes',NULL,NULL),(177,'Nose',NULL,NULL),(178,'Ears',NULL,NULL),(179,'Teeth',NULL,NULL);
/*!40000 ALTER TABLE `tblWords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'impactapp'
--

--
-- Dumping routines for database 'impactapp'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-16 16:59:28
